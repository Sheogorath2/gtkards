import json
import random
from collections import OrderedDict

from common import get_translations, DEFAULTMETA, METAKEY

class Controller:
    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.wasHint = False
        self.local=None

    def set_dict_from_json(self, json_dict, json_meta=None):
        dict = OrderedDict(json.loads(json_dict))
        try:
            try:
                local = json.JSONDecoder(object_pairs_hook=OrderedDict).decode(json_meta)["local"]
            except TypeError:
                local=None
            meta = json.JSONDecoder(object_pairs_hook=OrderedDict).decode(json_meta)["meta"]
        except TypeError:
            meta=None
        self.set_localization(local)
        self.set_dict(dict, meta)

    def set_localization(self, local):
        self.local=local

    def get_localization(self):
        return self.local

    def set_dict(self, data, meta=None):
        self.model.cards = data
        self.model.meta = meta

    def get_meta(self):
        return self.model.meta

    def set_meta(self,meta):
        self.model.meta=meta

    def set_meta_param(self,param,value=None):
        if (self.model.meta==None):
            self.set_meta(OrderedDict(DEFAULTMETA[METAKEY]))

        self.model.meta[param]=value

    def get_cards(self):
        return self.model.cards

    def get_card(self):
        return self.card

    def _next_card(self):
        self.card = random.choice(list(self.model.cards.items()))

    def next(self):
        self._next_card()
        self.wasHint = False
        self.view.card_updated(self.card)

    def checktranslation(self, translation):
        translations = get_translations(self.card)
        return True if translation.lower() in translations \
            else False

    def get_translations_of_card(self):
        return get_translations(self.card)
