# !/usr/bin/env python3

from controller import Controller
from model import Model
from view import View
from common import DEFAULTCARD, DEFAULTMETA

model = Model(DEFAULTCARD, DEFAULTMETA)
view = View()
controller = Controller(model=model, view=view)
view.register_observer(controller)
view.run()