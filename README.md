Flashcard program for learning foreign words.
Uses pygi library.

[![SourceForge project page](http://media.indiedb.com/cache/images/groups/1/5/4774/thumb_620x2000/sourceforge.png)](http://gtkards.sf.net)
     

### Download latest: ###
 [![Download GTKards](https://img.shields.io/sourceforge/dm/gtkards.svg)](https://sourceforge.net/projects/gtkards/files/latest/download)

Run it using `python3 main.py`.
On windows in order to run it you should have [python3](http://python.org/downloads) and [PyGObject](https://sourceforge.net/projects/pygobjectwin32) installed.

### VERSIONS: ###
Version commits are tagged. You can find them in [downloads/tags](https://bitbucket.org/Sheogorath2/gtkards/downloads#tag-downloads). Also you can find them in [sourceforge project files](https://sourceforge.net/projects/gtkards/files/).

**VERSION 0.2**
Cards meta

**VERISON 0.1**
Just plain word cards