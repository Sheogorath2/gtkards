import gi
import traceback

from common import get_translations, DEFAULTMETA

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class View(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.builder = Gtk.Builder()
        self.builder.add_from_file("test.glade")
        self.builder.connect_signals(self)

        self.lblWord = self.builder.get_object("lblWord")
        self.lblTranscription = self.builder.get_object("lblTranscription")
        self.inputTrans = self.builder.get_object("inputTrans")
        self.lblMessage = self.builder.get_object("lblMessage")
        self.focus_input()
        self.lblMessage.set_text("Type translation below")

        self.msgCorrect="Correct!"
        self.msgIncorrect="Incorrect!"

    def register_observer(self, controller):
        self.controller = controller

    def run(self, *args):
        window = self.builder.get_object("window1")
        window.show()
        self.controller.next()
        window.connect('destroy', Gtk.main_quit)
        Gtk.main()

    def card_updated(self,card):
        self.show_card(card)
        self.inputTrans.set_text("")

    def show_card(self, card):
        self.lblWord.set_text(card[0])
        self.lblTranscription.set_text(card[1][0])
        #self.inputTrans.set_text(card[1][1])

    def quit(self, *args):
        Gtk.main_quit()

    def hint(self, *args, userskip=False):
        translations=get_translations(self.controller.get_card())
        text=""
        for translation in translations:
            if(text!=""):
                text+="; "
            text+=translation
        self.lblMessage.set_text(text)
        self.wasHint=True
        self.focus_input()

    def check(self, *args):
        isCorrect=self.controller.checktranslation(self.inputTrans.get_text().lower())
        if (not isCorrect):
            self.lblMessage.set_text(self.msgIncorrect)
            self.focus_input()
            return
        self.lblMessage.set_text(self.msgCorrect)
        self.focus_input()
        self.controller.next()

    def focus_input(self):
        self.inputTrans.grab_focus()

    def open_dict(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())

            try:
                fname_dict=dialog.get_filename()
                fname_meta=fname_dict+".meta"
                dict_text=open(fname_dict, encoding='utf-8').read()
                hasmeta=True
                try:
                    meta_text=open(fname_meta, encoding='utf-8').read()
                except FileNotFoundError:
                    meta_text=None
                    hasmeta=False
                self.controller.set_dict_from_json(dict_text,meta_text)
                self.controller.set_meta_param("path", fname_dict)
                self.controller.set_meta_param("hasMeta", str(hasmeta))
                if(hasmeta):
                    local=self.controller.get_localization()
                    if(local!=None):
                        self.msgCorrect=local["msgCorrect"]
                        self.msgIncorrect=local["msgIncorrect"]

            except Exception:
                self.show_error_dialog_with_traceback("error opening file")

        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()
        self.lblMessage.set_text("")

        self.controller.next()

    def add_filters(self, dialog):
        filter_json = Gtk.FileFilter()
        filter_json.set_name("JSON files")
        filter_json.add_pattern("*.json")
        dialog.add_filter(filter_json)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    def dictinfo(self, widget):
        dictinfo=self.controller.get_meta()

        message="No meta info avaliable."
        if (dictinfo!=None and dictinfo!=DEFAULTMETA):
            message=""
            for key in dictinfo.keys():
                value=dictinfo[key]
                message+=key
                if (value!=None):
                    message+=": "+value
                message+="\n"

        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "Dictionary")
        dialog.format_secondary_text(
            message)
        dialog.run()

        dialog.destroy()

    def show_error_dialog_with_traceback(self,message,printstacktrace=True):
        if (printstacktrace):
            traceback.print_exc()
        self.show_error_dialog(message,traceback.format_exc())

    def show_error_dialog(self,message,secondary):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
            Gtk.ButtonsType.CANCEL, message)
        dialog.format_secondary_text(
            secondary)
        dialog.run()
        print("ERROR dialog closed")

        dialog.destroy()